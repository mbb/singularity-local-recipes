## Building local recipes from docker or singularity recipes

To be able to build directly an image:

```bash
apptainer build lolcow.img <recipe.def>

# with singularity
sudo singularity build lolcow.img <recipe.def>
```

And to build a writable temporary image, you would need a [sandbox](https://apptainer.org/docs/user/main/quick_start.html#sandbox-directories) (useful for build time):

```bash
apptainer build --sandbox alpine/ docker://alpine
apptainer exec --writable alpine touch /foo

# with singularity
sudo singularity build --sandbox alpine/ docker://alpine
singularity exec --writable alpine touch /foo
```

From https://apptainer.org/docs/user/main/quick_start.html#apptainer-definition-files
