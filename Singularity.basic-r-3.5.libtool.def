Bootstrap: docker
From: ubuntu:18.04
IncludeCmd: yes


%environment
  export R_VERSION=3.5.3
  export R_CONFIG_DIR=/etc/R/
  export PATH=/usr/local/bin:~/R/x86_64-pc-linux-gnu-library/3.4/snow:${PATH}
  export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/
  export JDK_HOME=/usr/lib/jvm/java-8-openjdk-amd64/
  export JRE_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre
  export LD_LIBRARY_PATH=/usr/local/lib:/usr/lib64:${LD_LIBRARY_PATH}
  export R_HOME=/usr/lib64/R
  export RHOME=/usr/lib64/R
  export LANG=en_US.UTF-8
  export LANGUAGE=en_US:en
  export LC_ALL=en_US.UTF-8


%labels
  Author Remy Dernat
  Version v0.0.3
  R_Version 3.5.3
  build_date 2021 May 20
  build_last_update 2021 May 20

#%files
#  libibverbs.d/ /etc/

%post
  NPROCS=`awk '/^processor/ {s+=1}; END{print s}' /proc/cpuinfo`
  PATH=/usr/local/bin:${PATH}
  LD_LIBRARY_PATH=/usr/local/lib:/usr/lib64:${LD_LIBRARY_PATH}

  # generic packages
  apt-get update --fix-missing
  apt-get install -qqy software-properties-common apt-transport-https \
    build-essential locales dirmngr
  apt-get install -qqy liblzma-dev libdapl2

  # Timezone and locales settings
  TZ=Europe/Paris
  ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
  #dpkg-reconfigure -f noninteractive tzdata
  sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
  locale-gen
  echo 'LANG="en_US.UTF-8' >> /etc/default/locale

  # R repository to get the last R release
  apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
  add-apt-repository "deb http://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/"

  apt-get update
  apt-get install -qqy r-base r-base-dev
  apt-get install -qqy libblas3 libblas-dev liblapack-dev liblapack3
  apt-get install -qqy openssh-client openssh-server
  apt-get install -qqy openjdk-8-jre
  apt-get install -qqy libtool libc6-dev


  # Specific Cluster config
  mkdir -p /share/apps/bin
  mkdir /share/apps/lib
  mkdir /share/apps/gridengine
  mkdir /share/bio
  mkdir -p /opt/gridengine
  mkdir -p /export/scrach
  mkdir -p /usr/lib64
  /usr/sbin/groupadd --system --gid 400 sge
  /usr/sbin/useradd --system --uid 400 --gid 400 -c GridEngine --shell /bin/true --home /opt/gridengine sge

  ln -s /usr/lib/jvm/java-8-openjdk-amd64 /usr/lib/jvm/default-java
  #ln -s /library/littler/bin/r /usr/local/bin/r
  ln -s /bin/bash /bin/mbb_bash
  ln -s /bin/bash /bin/isem_bash
  ln -s /usr/lib/x86_64-linux-gnu/libdat2.so.2 /usr/lib64/libdat.so.1
  ln -s /usr/lib/R /usr/lib64/R

  ## MPI stuffs (installing the same OpenMPI version that the one on the nodes)
  apt-get install -y libibverbs1 libibverbs-dev ibverbs-utils libmlx4-1 libmthca1
  cd /usr/local
  #wget -O /tmp/openmpi-1.6.2.tar.gz https://www.open-mpi.org/software/ompi/v1.6/downloads/openmpi-1.6.2.tar.bz2
  wget -O /tmp/openmpi-4.0.1.tar.gz https://download.open-mpi.org/release/open-mpi/v4.0/openmpi-4.0.1.tar.gz
  tar -xf /tmp/openmpi-4.0.1.tar.gz
  cd openmpi-4.0.1
  ./configure --prefix=/usr/local --enable-static
  make -j ${NPROCS}
  make install

  # library updates using ldconfig
  echo -e "/usr/local/lib\n/usr/lib64" > /etc/ld.so.conf.d/locallibs.conf
  echo /usr/local/lib/openmpi > /etc/ld.so.conf.d/ompi.conf
  echo /usr/lib/lapack > /etc/ld.so.conf.d/lapack.conf
  #echo /usr/lib/atlas-base > /etc/ld.so.conf.d/atlas.conf
  echo /usr/lib/libblas > /etc/ld.so.conf.d/blas.conf
  /sbin/ldconfig

  ## fixing locales in R
  #echo 'LANG <- Sys.getenv("LANG")' | R --slave
  #echo 'Sys.setlocale("LC_ALL", LANG)' | R --slave

  ## installing some packages
  echo install.packages\(\"lme4\"\, repos\=\'https://cloud.r-project.org\'\) | R --slave
  echo install.packages\(\"ape\"\, repos\=\'https://cloud.r-project.org/\'\) | R --slave
  #echo install.packages\(\"FD\"\, repos\=\'https://cloud.r-project.org/\'\) | R --slave
  # Rmpi need to be install by hand
  # see: https://www.sharcnet.ca/help/index.php/Using_R_and_MPI
  # eg:
  # wget https://cran.r-project.org/src/contrib/Rmpi_0.6-9.tar.gz
  # OPENMPI=/share/apps/bin/openmpi/4.0.1
  # R CMD INSTALL --configure-args="--with-Rmpi-include=$OPENMPI/include   --with-Rmpi-libpath=$OPENMPI/lib --with-Rmpi-type='OPENMPI' " Rmpi_0.6-9.tar.gz
  #echo install.packages\(\"doMPI\"\, repos\=\'https://cloud.r-project.org/\'\) | R --slave


%apprun R
  exec R "$@"

%apprun Rscript
  exec Rscript "$@"

%runscript
  exec R "$@"

%help
  This is a container with R-3.5 adapted for the MBB Cluster
